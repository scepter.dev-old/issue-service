ARG NODE_VERSION=12

FROM node:12.16 AS builder

COPY . .

RUN npm install

EXPOSE 3001

CMD ["npm", "run", "start"]
