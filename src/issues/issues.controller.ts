import {Body, Controller, ForbiddenException, Get, Post, Req, Res} from '@nestjs/common';
import {IssuesService} from "./issues.service";
import CreateIssueDTO from "../dto/CreateIssueDTO";
@Controller('issues')
export class IssuesController {
    constructor(private readonly issuesService: IssuesService) {}

    @Get('')
    getAllIssues(@Req() req, @Res() res) {
        if(!req.headers.user){
            throw new ForbiddenException();
        }

        return this.issuesService.getIssues(req.headers.user, req.query).then(issues => {
            return res.status(200).json(issues);
        })
    }

    @Post('')
    createIssue(@Req() req, @Res() res, @Body() issue: CreateIssueDTO) {
        if(!req.headers.user){
            throw new ForbiddenException();
        }

        //TODO check if user has access to project via creator or member

        return this.issuesService.createIssue(issue.caption, issue.description, issue.project, req.headers.user).then(issue => {
           return res.status(200).json(issue);
        });
    }

}
