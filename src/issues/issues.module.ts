import { HttpModule, Module } from "@nestjs/common";
import { IssuesController } from './issues.controller';
import { IssuesService } from './issues.service';

@Module({
  imports: [HttpModule],
  controllers: [IssuesController],
  providers: [IssuesService]
})
export class IssuesModule {}
