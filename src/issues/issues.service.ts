import { HttpService, Injectable } from "@nestjs/common";
import {Issue} from "../entity/Issue";
import {Connection} from "typeorm";

@Injectable()
export class IssuesService {
    constructor(private connection: Connection, private httpService: HttpService) {}

    async getIssues(user: string, query: any) {
        let builder = await this.connection.getRepository("issue").createQueryBuilder("issue")
            .leftJoinAndMapMany('issue.assignees', 'issue.assignees', 'assignee');


        if(query.assignee) {
            if(query.assignee === 'me') {
                query.assignee = user;
            }
            if(Array.isArray(query.assignee)) {
                builder = builder.andWhere('assignee.user IN (:...users)', {users: query.assignee});
            } else {
                console.log("test");
                builder = builder.andWhere('assignee.user IN (:...users)', {users: [query.assignee]});
            }

            console.log(builder.getSql());
        }

        if(query.author) {
            builder = builder.andWhere('author = :author', {author: query.author});
        }

        if(query.project){
            if(Array.isArray(query.project)) {
                builder = builder.andWhere('project IN (:...projects)', {projects: query.project});
            } else {
                builder = builder.andWhere('project = :project', {project: query.project});
            }
        }


        const issues = await builder.getMany().then(response => {
            return response;
        });

        const projectIds = [];
        const userIds = [];

        issues.forEach(issue => {
            if(!projectIds.includes(issue["project"])) {
                projectIds.push(issue["project"]);
            }
            issue["assignees"].forEach(assignee => {
                if(!userIds.includes(assignee["user"])) {
                    userIds.push(assignee["user"]);
                }
            });
        });

        return issues;
    }
    async getAllAssignedIssues(user: string) {
        return await this.connection.getRepository("issue").createQueryBuilder("issue")
            .leftJoinAndMapMany('issue.assignees', 'issue.assignees', 'assignee')
            .where('assignee.user = :user', {user})
            .getMany().then(response => {
                return response;
            });
    }
    async createIssue(caption: string, description: string, project: string, author: string) {
        const issue = new Issue();
        issue.caption = caption;
        issue.description = description;
        issue.project = project;
        issue.author = author;
        await issue.save();
        return issue;
    }
}
