import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { IssuesModule } from './issues/issues.module';
import {TypeOrmModule} from "@nestjs/typeorm";
import * as config from "./ormConfig";

@Module({
  imports: [
    TypeOrmModule.forRoot(config),
    IssuesModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
