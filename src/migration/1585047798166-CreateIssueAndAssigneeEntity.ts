import {MigrationInterface, QueryRunner} from "typeorm";

export class CreateIssueAndAssigneeEntity1585047798166 implements MigrationInterface {
    name = 'CreateIssueAndAssigneeEntity1585047798166'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("CREATE TABLE `issue` (`id` varchar(36) NOT NULL, `caption` varchar(255) NOT NULL, `description` text NOT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB", undefined);
        await queryRunner.query("CREATE TABLE `assignee` (`id` varchar(36) NOT NULL, `user` varchar(255) NOT NULL, `issueId` varchar(36) NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB", undefined);
        await queryRunner.query("ALTER TABLE `assignee` ADD CONSTRAINT `FK_5b17aa701513bf9dd8c5f33c9a3` FOREIGN KEY (`issueId`) REFERENCES `issue`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION", undefined);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("ALTER TABLE `assignee` DROP FOREIGN KEY `FK_5b17aa701513bf9dd8c5f33c9a3`", undefined);
        await queryRunner.query("DROP TABLE `assignee`", undefined);
        await queryRunner.query("DROP TABLE `issue`", undefined);
    }

}
