import {MigrationInterface, QueryRunner} from "typeorm";

export class UpdateIssueEntity1585063711165 implements MigrationInterface {
    name = 'UpdateIssueEntity1585063711165'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("ALTER TABLE `issue` ADD `author` varchar(255) NOT NULL", undefined);
        await queryRunner.query("ALTER TABLE `issue` ADD `project` varchar(255) NOT NULL", undefined);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("ALTER TABLE `issue` DROP COLUMN `project`", undefined);
        await queryRunner.query("ALTER TABLE `issue` DROP COLUMN `author`", undefined);
    }

}
