import {BaseEntity, Column, Entity, OneToMany, PrimaryGeneratedColumn} from "typeorm";
import {Assignee} from "./Assignee";

@Entity()
export class Issue extends BaseEntity{

    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column('varchar', {length: 255, nullable: false})
    caption: string;

    @Column('text')
    description: string;

    @Column('varchar')
    author: string;

    @Column('varchar')
    project: string;

    @OneToMany(type => Assignee, assignee => assignee.issue, {cascade: true})
    assignees: Assignee[]
}
