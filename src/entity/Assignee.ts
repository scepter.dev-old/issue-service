import {BaseEntity, Column, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn} from "typeorm";
import {Issue} from "./Issue";

@Entity()
export class Assignee extends BaseEntity{

    @PrimaryGeneratedColumn('uuid')
    id: string;

    @ManyToOne(type => Issue, issue => issue.assignees, {onDelete: 'CASCADE'})
    issue: Issue;

    @Column('varchar')
    user: string;

}
