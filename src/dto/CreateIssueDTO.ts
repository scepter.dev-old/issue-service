class CreateIssueDTO {
    caption: string;
    description: string;
    project: string;
}


export default CreateIssueDTO;
